# Test makefile for makemake

# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

# Use absolute path so tests that change working directory still use
# scripts from parent directory.  Note that using $PWD seems to fail
# here under Gitlab's CI environment.
PARDIR=$(realpath ..)
PATH := $(PARDIR):$(realpath .):${PATH}

# Find all *.mk entries to test
MAKEMAKE_LOADS := $(shell ls -1 *.mk | sed '/.mk/s///' | sort)
DECONFIG_LOADS := $(shell ls -d *-project | sed '/-project/s///' | sort)

.PHONY: cHeck clean testlist listcheck buildchecks

check:
	@make tap | ./tapview

.SUFFIXES: .chk

clean:
	rm -fr *~

# Show summary lines for all tests.
testlist:
	@grep '^##' *.mk
listcheck:
	@for f in *.mk; do \
	    if ( head -3 $$f | grep -q '^ *##' ); then :; else echo "$$f needs a description"; fi; \
	done

# Rebuild characterizing tests
buildchecks:
	@for file in $(MAKEMAKE_LOADS); do \
	    echo "Remaking $${file}.chk"; \
	    OPTS=`sed -n /#options:/s///p <$${file}.mk`; \
	    makemake -q $$OPTS $${file}.mk >$${file}.chk 2>&1 || exit 1; \
	done;
	@for stem in $(DECONFIG_LOADS); do \
	    echo "Remaking $${stem}-project.chk"; \
	    ./test_deconfig "$${stem}" >"$${stem}-project.chk"; \
	done;

RUN_TARGETS=$(MAKEMAKE_LOADS:%=run-regress-%)
$(RUN_TARGETS): run-regress-%: %.mk
	@(test=$(<:.mk=); legend=$$(sed -n '/^## /s///p' <"$<" 2>/dev/null || echo "(no description)"); \
	OPTS=`sed -n /#options:/s///p $<`; \
	makemake -q $$OPTS $< | ./tapdiffer "$${test}: $${legend}" "$${test}.chk")

IDEMPOTENCY_TARGETS=$(MAKEMAKE_LOADS:%=check-idempotency-%)
$(IDEMPOTENCY_TARGETS): check-idempotency-%: %.chk
	@(test=$(<:.chk=); legend=$$(sed -n '/^## /s///p' <"$<" 2>/dev/null || echo "(no description)"); \
	makemake -q $< | ./tapdiffer "$${test}: $${legend} idempotency" $<)

DECONFIG_TARGETS=$(DECONFIG_LOADS:%=deconfig-regress-%)
$(DECONFIG_TARGETS): deconfig-regress-%: %-project
	@(stem=$(<:-project=); ./test_deconfig $${stem} | ./tapdiffer "$${stem}: deconfig test" "$<.chk")

foo:
	echo $(DECONFIG_TARGETS)

SHELL_LOADS := onepass
SHELL_TARGETS = $(SHELL_LOADS:%=shell-test-%)
$(SHELL_TARGETS): shell-test-%:
	@$(SHELL) $*.sh

amhello-normal:
	@./test_makemake amhello

amhello-undo:
	@./test_makemake -u amhello

giflib-normal:
	@./test_makemake giflib

PROJECT_TARGETS = amhello-normal amhello-undo giflib-normal

TEST_TARGETS = $(RUN_TARGETS) $(IDEMPOTENCY_TARGETS) $(SHELL_TARGETS) $(PROJECT_TARGETS) $(DECONFIG_TARGETS)

tap: count $(TEST_TARGETS)
count:
	@echo 1..$(words $(TEST_TARGETS))

